const fs = require("fs");

const glassHelper = fs.readFileSync("./helpers/glass.json").toString('UTF-8');
const domains = fs.readdirSync("domains");
const trustHelper = fs.readFileSync("./helpers/trust.json").toString('UTF-8');
const domainHelper = fs.readFileSync("./helpers/domain.json").toString('UTF-8');

const environments = ["dev", "tst"];
const services = ["id", "tax", "passport", "health", "education"];
let envSuffix = "";

function init() {

  for(let key of environments) {
    envSuffix = key;
    fs.mkdirSync(`bdns/${envSuffix}`, {recursive: true});
    
      domains.forEach((key) => {
        // const endpoint = fs.readFileSync(`domains/${key}`).toString('UTF-8');
        const domain = key.replace('.txt', '');
        prepareData(domain);
      });
  }
}

function prepareData(domain) {

  const data =  JSON.parse(replace(glassHelper, 'DOMAIN', domain));
  const additionalData = getAdditionalData(domain); 

  return output(domain, {
    ...data,
    ...additionalData
  });
}

function getAdditionalData(domainToSkip) {
  let data = {};

  domains.forEach((key) => { 
    let endpoint = fs.readFileSync(`domains/${key}`).toString('UTF-8');
    const domain = key.replace('.txt', '');
    let env = envSuffix == 'dev' ? (domain === 'glass' ? 'dev.' : '-dev') : '';

    if(domain !== domainToSkip) {
      endpoint = replace(endpoint, 'ENV_SUFFIX', env);
      let aData = replace(domainHelper, 'DOMAIN', domain);
      data = Object.assign(data, JSON.parse(replace(aData, 'ENDPOINT', endpoint)));
    }

  });
 
  return data;   
}


function replace(data, key, value) {
  if(value === 'glass' && envSuffix === 'dev')
    value = `${value}.dev`;

  if(value.includes('trust') || value.includes('mobile'))
    data = trustHelper;

  return data.replace(new RegExp(`%{${key}}`, 'gi'), value); 
}

function output(host, data) {
  return fs.writeFileSync(`bdns/${envSuffix}/${host}.json`, JSON.stringify(data, null, 2), { encoding:'utf8', flag: 'w' });
}

init();  
 
  